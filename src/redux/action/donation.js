import Axios from 'axios';
import { API_HOST } from '../../config';
import { getData, showMessage } from '../../utils';
import { setLoading } from './global';

export const storeDonation = (data, navigation) => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.post(`${API_HOST.url}/donation`, data, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        navigation.reset({ index: 0, routes: [{ name: 'SplashDone' }] });
      })
      .catch(err => {
        dispatch(setLoading(false));
        showMessage(err?.response?.data?.data?.message);
      });
  });
};
