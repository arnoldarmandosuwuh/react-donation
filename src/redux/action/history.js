import Axios from 'axios';
import { API_HOST } from '../../config';
import { getData, showMessage } from '../../utils';
import { setLoading } from './global';

export const getMenunggu = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/donation-histories?status=1`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_MENUNGGU', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};

export const getDiproses = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/donation-histories?status=2`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_DIPROSES', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};

export const getBerhasil = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/donation-histories?status=3`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_BERHASIL', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};

export const getDibatalkan = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/donation-histories?status=4`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_DIBATALKAN', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};
