import Axios from 'axios';
import { API_HOST } from '../../config';
import { getData, showMessage } from '../../utils';
import { setLoading } from './global';

export const fetchQuantity = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/quantity`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_QUANTITY', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};
