import Axios from 'axios';
import { API_HOST } from '../../config';
import { getData, showMessage } from '../../utils';
import { setLoading } from './global';

export const fetchStatus = () => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    Axios.get(`${API_HOST.url}/donation-status`, {
      headers: {
        Authorization: resToken.value,
      },
    })
      .then(res => {
        dispatch(setLoading(false));
        dispatch({ type: 'SET_STATUS', value: res.data.data });
      })
      .catch(err => {
        showMessage(err?.response?.data?.data?.message);
      });
  });
};
