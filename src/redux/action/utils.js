import Axios from 'axios';
import { API_HOST } from '../../config';
import { showMessage } from '../../utils';
import { setLoading } from './global';

export const fetchProvinces = () => dispatch => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/regions/province`)
    .then(res => {
      dispatch(setLoading(false));
      dispatch({ type: 'SET_PROVINCES', value: res.data.data });
    })
    .catch(err => {
      showMessage(err?.response?.data?.data?.message);
    });
};

export const fetchCities = id => dispatch => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/regions/cities/${id}`)
    .then(res => {
      dispatch(setLoading(false));
      dispatch({ type: 'SET_CITIES', value: res.data.data });
    })
    .catch(err => {
      showMessage(err?.response?.data?.data?.message);
    });
};

export const fetchDistricts = id => dispatch => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/regions/districts/${id}`)
    .then(res => {
      dispatch(setLoading(false));
      dispatch({ type: 'SET_DISTRICTS', value: res.data.data });
    })
    .catch(err => {
      showMessage(err?.response?.data?.data?.message);
    });
};

export const fetchVillages = id => dispatch => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/regions/villages/${id}`)
    .then(res => {
      dispatch(setLoading(false));
      dispatch({ type: 'SET_VILLAGES', value: res.data.data });
    })
    .catch(err => {
      showMessage(err?.response?.data?.data?.message);
    });
};
