const initItemState = {
  items: [],
};

export const itemsReducer = (state = initItemState, action) => {
  if (action.type === 'SET_ITEM') {
    return {
      ...state,
      items: action.value,
    };
  }

  return state;
};
