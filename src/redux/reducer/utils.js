const initUtilsState = {
  provinces: [],
  cities: [],
  districts: [],
  villages: [],
};

export const utilsReducer = (state = initUtilsState, action) => {
  if (action.type === 'SET_PROVINCES') {
    return {
      ...state,
      provinces: action.value,
    };
  }

  if (action.type === 'SET_CITIES') {
    return {
      ...state,
      cities: action.value,
    };
  }

  if (action.type === 'SET_DISTRICTS') {
    return {
      ...state,
      districts: action.value,
    };
  }

  if (action.type === 'SET_VILLAGES') {
    return {
      ...state,
      villages: action.value,
    };
  }

  return state;
};
