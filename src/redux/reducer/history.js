const initHistoryState = {
  menunggu: [],
  diproses: [],
  berhasil: [],
  dibatalkan: [],
};

export const historyReducer = (state = initHistoryState, action) => {
  if (action.type === 'SET_MENUNGGU') {
    return {
      ...state,
      menunggu: action.value,
    };
  }
  if (action.type === 'SET_DIPROSES') {
    return {
      ...state,
      diproses: action.value,
    };
  }
  if (action.type === 'SET_BERHASIL') {
    return {
      ...state,
      berhasil: action.value,
    };
  }
  if (action.type === 'SET_DIBATALKAN') {
    return {
      ...state,
      dibatalkan: action.value,
    };
  }
  return state;
};
