const initStatusState = {
  status: [],
};

export const statusReducer = (state = initStatusState, action) => {
  if (action.type === 'SET_STATUS') {
    return {
      ...state,
      status: action.value,
    };
  }

  return state;
};
