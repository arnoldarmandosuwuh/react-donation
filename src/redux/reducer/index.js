import { combineReducers } from 'redux';
import { globalReducer } from './global';
import { fetchProfileReducer, photoReducer, registerReducer } from './auth';
import { utilsReducer } from './utils';
import { termsReducer } from './terms';
import { statusReducer } from './status';
import { quantityReducer } from './quantity';
import { categoryReducer } from './category';
import { itemsReducer } from './item';
import { historyReducer } from './history';

const reducer = combineReducers({
  registerReducer,
  globalReducer,
  fetchProfileReducer,
  utilsReducer,
  photoReducer,
  termsReducer,
  statusReducer,
  quantityReducer,
  categoryReducer,
  itemsReducer,
  historyReducer,
});

export default reducer;
