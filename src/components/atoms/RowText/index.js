import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { colors } from '../../../utils';

const RowText = ({ title, deskripsi, value, link, onPress}) => {
  if (title) {
    return <Text style={styles.title}>{title}</Text>;
  }
  return (
    <View style={styles.container}>
      <Text style={styles.deskripsi}>{deskripsi}</Text>
      {
        link ? (
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={onPress}
          >
            <Text style={styles.link}>{value}</Text>
          </TouchableOpacity>
        ) : (
          <Text style={styles.value}>{value}</Text>
        )
      }
    </View>
  );
};
export default RowText;

const styles = StyleSheet.create({
  link: {
    fontFamily: 'Poppins-Medium',
    color: colors.sixth,
    fontSize: 14
  },
  container: {
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom: 15,
    margin: 0,
  },
  title: {
    fontSize: 16,
    color: colors.primary,
    fontFamily: 'Poppins-Medium',
  },
  deskripsi: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
    flex: 1,
  },
  value: {
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: colors.primary,
  },
});
