import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, Text, View, Image } from 'react-native';
import { mikasa } from '../../../assets';
import { colors, getData } from '../../../utils';
import moment from 'moment';

const ProfileHeader = () => {
  const navigation = useNavigation();
  const [photo, setPhoto] = useState(mikasa);
  const [name, setName] = useState('mikasa');

  useEffect(() => {
    navigation.addListener('focus', () => {
      getData('userProfile').then(res => {
        setPhoto({ uri: res.foto });
        setName(res.name);
      });
    });
  }, [navigation]);

  return (
    <>
      <View style={styles.page}>
        <View style={styles.text}>
          <Text style={styles.tgl}>{moment().format('dddd, D MMMM YYYY')}</Text>
          <Text style={styles.nama}>Hai, {name}</Text>
        </View>
        <View style={styles.photo}>
          <Image source={photo} style={styles.image} />
        </View>
      </View>
    </>
  );
};
export default ProfileHeader;

const styles = StyleSheet.create({
  tgl: {
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    color: colors.primary,
  },
  nama: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: colors.primary,
  },
  photo: {
    marginLeft: 20,
  },
  text: {
    flex: 1,
  },
  page: {
    flexDirection: 'row',
    paddingTop: 30,
    paddingBottom: 24,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50 * 50,
  },
});
