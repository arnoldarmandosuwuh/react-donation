import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  RefreshControl,
  ScrollView,
} from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { useDispatch, useSelector } from 'react-redux';
import {
  getDiproses,
  getMenunggu,
  getBerhasil,
  getDibatalkan,
} from '../../../redux/action';
import { colors } from '../../../utils';
import CardProses from '../CardProses';

const renderTabBar = props => (
  <TabBar
    {...props}
    indicatorStyle={styles.indicator}
    style={styles.tabBarStyle}
    tabStyle={styles.tabStyle}
    scrollEnabled
    renderLabel={({ route, focused }) => (
      <Text style={styles.tabText(focused)}>{route.title}</Text>
    )}
  />
);

const Menunggu = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { menunggu } = useSelector(state => state.historyReducer);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    dispatch(getMenunggu());
  }, [dispatch]);

  const onRefresh = () => {
    setRefreshing(true);
    dispatch(getMenunggu());
    setRefreshing(false);
  };

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <View style={styles.page}>
        <View style={styles.card}>
          {menunggu.map(item => {
            return (
              <CardProses
                key={item.id}
                title={item.nama_barang}
                banyak={item.total_items}
                tgl={moment(item.donation_date).format('D MMMM YYYY')}
                status="#C4C4C4"
                onPress={() => navigation.navigate('DetailHistory', item.id)}
              />
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

const Diproses = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { diproses } = useSelector(state => state.historyReducer);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
      dispatch(getDiproses());
  }, [dispatch]);

  const onRefresh = () => {
    setRefreshing(true);
    dispatch(getDiproses());
    setRefreshing(false);
  };

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <View style={styles.page}>
        <View style={styles.card}>
          {diproses.map(item => {
            return (
              <CardProses
                key={item.id}
                title={item.nama_barang}
                banyak={item.total_items}
                tgl={moment(item.donation_date).format('D MMMM YYYY')}
                status="#FBBD39"
                onPress={() => navigation.navigate('DetailHistory', item.id)}
              />
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

const Dibatalkan = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { dibatalkan } = useSelector(state => state.historyReducer);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
      dispatch(getDibatalkan());
  }, [dispatch]);

  const onRefresh = () => {
    setRefreshing(true);
    dispatch(getDibatalkan());
    setRefreshing(false);
  };

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <View style={styles.page}>
        <View style={styles.card}>
          {dibatalkan.map(item => {
            return (
              <CardProses
                key={item.id}
                title={item.nama_barang}
                banyak={item.total_items}
                tgl={moment(item.donation_date).format('D MMMM YYYY')}
                status="#FB3939"
                onPress={() => navigation.navigate('DetailHistory', item.id)}
              />
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

const Berhasil = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { berhasil } = useSelector(state => state.historyReducer);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
      dispatch(getBerhasil());
  }, [dispatch, navigation]);

  const onRefresh = () => {
    setRefreshing(true);
    dispatch(getBerhasil());
    setRefreshing(false);
  };

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <View style={styles.page}>
        <View style={styles.card}>
          {berhasil.map(item => {
            return (
              <CardProses
                key={item.id}
                title={item.nama_barang}
                banyak={item.total_items}
                tgl={moment(item.donation_date).format('D MMMM YYYY')}
                status="#6BFB39"
                onPress={() => navigation.navigate('DetailHistory', item.id)}
              />
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};
const initialLayout = { width: Dimensions.get('window').width };

const HomeTabSelection = () => {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: '1', title: 'Menunggu' },
    { key: '2', title: 'Diproses' },
    { key: '3', title: 'Dibatalkan' },
    { key: '4', title: 'Berhasil' },
  ]);

  const renderScene = SceneMap({
    1: Menunggu,
    2: Diproses,
    3: Dibatalkan,
    4: Berhasil,
  });

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      style={styles.tabView}
    />
  );
};
export default HomeTabSelection;

const styles = StyleSheet.create({
  tabBarStyle: {
    backgroundColor: 'white',
    elevation: 0,
    shadowOpacity: 0,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
  },
  tabView: {
    backgroundColor: 'white',
  },
  page: {
    backgroundColor: colors.tertiary,
    paddingHorizontal: 24,
    paddingTop: 8,
  },
  tabText: focused => ({
    fontFamily: 'Poppins-Medium',
    color: focused ? '#020202' : '#8D92A3',
  }),
  indicator: {
    backgroundColor: colors.primary,
    height: 3,
    width: '15%',
    marginLeft: '3%',
  },
  tabStyle: { width: 'auto' },
});
