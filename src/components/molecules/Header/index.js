import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { IcBack } from '../../../assets';
import { colors } from '../../../utils';
const Header = ({ title, deskripsi, onBack, center, grey, single }) => {
  if (center) {
    return (
      <View style={styles.conCenter}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }
  return (
    <View style={[styles.container, grey ? styles.bgGrey : styles.bgWhite]}>
      {onBack && (
        <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
          <View style={styles.icon}>
            <IcBack />
          </View>
        </TouchableOpacity>
      )}
      {
        single ? (
          <View style={styles.singleStyle}>
            <Text style={styles.title}>{title}</Text>
          </View>
        ) : (
          <View>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.deskripsi}>{deskripsi}</Text>
          </View>
        )
      }
    </View>
  );
};
export default Header;

const styles = StyleSheet.create({
  singleStyle: {
    flex: 1,
    justifyContent: 'center',
  },
  bgWhite: {
    backgroundColor: colors.tertiary,
  },
  bgGrey: {
    backgroundColor: colors.seventh
  },
  conCenter: {
    backgroundColor: colors.tertiary,
    paddingHorizontal: 24,
    paddingTop: 30,
    paddingBottom: 24,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  icon: {
    padding: 16,
    marginRight: 16,
    marginLeft: -10,
    justifyContent: 'center',
  },
  container: {
    flexDirection: 'row',
    paddingHorizontal: 24,
    paddingTop: 30,
    paddingBottom: 24,
  },
  deskripsi: {
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: colors.primary,
  },
  title: {
    fontSize: 22,
    color: colors.primary,
    fontFamily: 'Poppins-Medium',
  },
});
