import React from 'react';
import { StyleSheet, Text, View, useWindowDimensions } from 'react-native';
import { colors } from '../../../utils';

const CorouselItem = ({ item }) => {
  const { width } = useWindowDimensions();
  return (
    <View style={[styles.container, { width }]}>
      <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.description}>{item.description}</Text>
    </View>
  );
};
export default CorouselItem;

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontFamily: 'Poppins-Regular',
    // marginBottom: 10,
    color: colors.secondary,
    textAlign: 'center',
  },
  description: {
    fontFamily: 'Poppins-Light',
    color: colors.secondary,
    textAlign: 'center',
    paddingHorizontal: 64,
  },
  image: {
    flex: 0.7,
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
