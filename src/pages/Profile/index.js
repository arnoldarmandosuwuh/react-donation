import React, { useEffect, useState } from 'react';
import {
  Image,
  Modal,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { IcProfilDefault } from '../../assets';
import { Button, Gap } from '../../components';
import { signOutAction } from '../../redux/action';
import { colors, getData } from '../../utils';

const Profile = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [userProfile, setUserProfile] = useState({});
  const [photo, setPhoto] = useState(IcProfilDefault);

  useEffect(() => {
    navigation.addListener('focus', () => {
      updateProfile();
    });
  }, [navigation]);

  const updateProfile = () => {
    getData('userProfile').then(res => {
      setUserProfile(res);
      setPhoto({ uri: res.foto });
    });
  };

  const dispatch = useDispatch();

  const yes = () => {
    setModalVisible(!modalVisible);
    dispatch(signOutAction(navigation));
  };
  const no = () => {
    setModalVisible(!modalVisible);
  };
  return (
    <>
      <Modal animationType="none" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              Apakah anda yakin mau keluar dari akun?
            </Text>
            <View style={styles.btnStyle}>
              <View>
                <Pressable style={styles.button} onPress={yes}>
                  <Text style={styles.textStyle}>Ya</Text>
                </Pressable>
              </View>
              <View>
                <Gap width={14} />
              </View>
              <View>
                <Pressable style={styles.button} onPress={no}>
                  <Text style={styles.textStyle}>Tidak</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>

      <View style={styles.page}>
        <ScrollView>
          <View style={styles.photo}>
            <Image source={photo} style={styles.photoContainer} />
            <View style={styles.profile}>
              <Text style={styles.title}>Hai, {userProfile.name}</Text>
              <Text style={styles.desc}>{userProfile.email}</Text>
            </View>
          </View>
          <Button
            btn
            tittle={'Perbarui akun'}
            onPress={() => navigation.navigate('UpdateBiodata')}
          />
          <Button btn tittle={'Keluar'} onPress={() => setModalVisible(true)} />
        </ScrollView>
      </View>
    </>
  );
};
export default Profile;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(52,64,125,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 16,
  },
  btnStyle: {
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  button: {
    borderRadius: 10,
    paddingVertical: 12,
    elevation: 2,
    backgroundColor: colors.primary,
    width: 117,
    height: 42,
  },
  textStyle: {
    color: colors.secondary,
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  page: {
    flex: 1,
    backgroundColor: colors.tertiary,
    paddingHorizontal: 25,
  },
  photo: {
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 20,
  },
  photoContainer: {
    width: 90,
    height: 90,
    borderRadius: 90,
    backgroundColor: '#F0F0F0',
    padding: 24,
  },
  profile: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 15,
    color: colors.primary,
    fontFamily: 'Poppins-Medium',
  },
  desc: {
    fontSize: 11,
    fontFamily: 'Poppins-Light',
    color: colors.primary,
  },
});
