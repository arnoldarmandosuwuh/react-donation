import React, { useEffect, useState } from 'react';
import {
  Image,
  PermissionsAndroid,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useDispatch, useSelector } from 'react-redux';
import {
  IcCalender,
  IcRemoveImageProfile,
  IcAddImageProfil,
} from '../../assets';
import { Button, Gap, Header, TextInput } from '../../components';
import { colors, useForm, showMessage, getData } from '../../utils';
import moment from 'moment';
import {
  fetchProvinces,
  fetchCities,
  fetchDistricts,
  fetchVillages,
  updateProfileAction,
} from '../../redux/action';

const UpdateBiodata = ({ navigation }) => {
  const [hasPhoto, setHasPhoto] = useState(false);
  const [photo, setPhoto] = useState('');
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [year, setYear] = useState();
  const [profile, setProfile] = useState({});
  const [biodata, setBiodata] = useForm({
    email: '',
    name: '',
    no_telp: '',
    tempat_lahir: '',
    tanggal_lahir: '',
    alamat: '',
    provinsi: '',
    kota: '',
    kecamatan: '',
    kelurahan: '',
    rw: '',
    rt: '',
    kode_pos: '',
  });

  const { provinces, cities, districts, villages } = useSelector(
    state => state.utilsReducer,
  );
  const { photoReducer } = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    navigation.addListener('focus', () => {
      getData('userProfile').then(resMember => {
        setProfile(resMember);
        if (resMember.foto) {
          setPhoto({ uri: resMember.foto });
          setHasPhoto(true);
        }
      });
    });
    dispatch(fetchProvinces());
  }, [navigation, dispatch]);

  const getImage = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Allow to access photos',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        launchImageLibrary(
          { includeBase64: true, maxWidth: 200, maxHeight: 200, quality: 0.5 },
          response => {
            if (response.didCancel || response.errorCode) {
              showMessage('Anda tidak memilih photo');
            } else {
              const source = { uri: response.assets[0].uri };
              const dataImage = {
                uri: response.assets[0].uri,
                type: response.assets[0].type,
                name: response.assets[0].fileName,
              };
              setPhoto(source);
              setHasPhoto(true);
              dispatch({ type: 'SET_PHOTO', value: dataImage });
              dispatch({ type: 'SET_UPLOAD_STATUS', value: true });
            }
          },
        );
      } else {
        showMessage('Perangkat tidak mengizinkan di izinkan');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const handleConfirm = date => {
    setYear(moment(date).format('D MMMM YYYY'));
    setBiodata('tanggal_lahir', moment(date).format('YYYY-MM-DD'));
    hideDatePicker();
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const getCities = provinsi_id => {
    if (provinsi_id !== 0) {
      setBiodata('provinsi', provinsi_id);
      dispatch(fetchCities(provinsi_id));
    }
  };

  const getDistricts = cities_id => {
    if (cities_id !== 0) {
      setBiodata('kota', cities_id);
      dispatch(fetchDistricts(cities_id));
    }
  };

  const getVillages = kecamatan_id => {
    if (kecamatan_id !== '') {
      setBiodata('kecamatan', kecamatan_id);
      dispatch(fetchVillages(kecamatan_id));
    }
  };

  const onContinue = () => {
    dispatch(updateProfileAction(biodata, photoReducer, navigation));
  };
  return (
    <>
      <Header
        title="Perbarui Akun"
        deskripsi="Silahkan memperbarui akun"
        onBack={() => navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.container}>
          <TouchableOpacity style={styles.photo} onPress={getImage}>
            <View style={styles.borderPhoto}>
              <View>
                {!hasPhoto && (
                  <View style={styles.photoContainer}>
                    <IcAddImageProfil style={styles.icon} />
                  </View>
                )}
                {hasPhoto && (
                  <View>
                    <Image source={photo} style={styles.photoContainer} />
                    <IcRemoveImageProfile style={styles.icon} />
                  </View>
                )}
              </View>
            </View>
          </TouchableOpacity>
          <TextInput
            label="Email"
            placeholder="Masukkan Email"
            value={profile.email}
            editable
          />
          <Gap height={12} />
          <TextInput
            label="Nama Lengkap"
            placeholder="Masukkan Nama Lengkap"
            value={biodata.name}
            onChangeText={value => setBiodata('name', value)}
          />
          <Gap height={12} />
          <TextInput
            label="Tempat lahir"
            placeholder="Masukkan tempat lahir"
            value={biodata.tempat_lahir}
            onChangeText={value => setBiodata('tempat_lahir', value)}
          />
          <Gap height={12} />
          <View style={styles.tgl}>
            <View style={styles.inpTgl}>
              <TextInput
                label="Tanggal"
                placeholder="Pilih tanggal lahir"
                value={year}
                style={styles.center}
                editable={false}
              />
            </View>
            <View style={styles.calender}>
              <Gap height={40} />
              <TouchableOpacity style={styles.cal} onPress={showDatePicker}>
                <IcCalender />
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
            </View>
          </View>
          <Gap height={12} />
          <TextInput
            label="No. hp"
            placeholder="Masukkan nomer hp"
            keyboardType="numeric"
            value={biodata.no_telp}
            onChangeText={value => setBiodata('no_telp', value)}
          />
          <Gap height={12} />
          <TextInput
            label="Alamat Lengkap"
            placeholder="Masukkan Alamat lengkap"
            value={biodata.alamat}
            onChangeText={value => setBiodata('alamat', value)}
          />
          <Gap height={12} />
          <TextInput
            label="Provinsi"
            placeholder="Pilih Provinsi"
            value={biodata.provinsi}
            onValueChange={value => getCities(value)}
            selectProvinsi
            selectItem={provinces}
          />
          <Gap height={12} />
          <TextInput
            label="Kota / Kabupaten"
            placeholder="Pilih Kota/kabupaten"
            value={biodata.kota}
            onValueChange={value => getDistricts(value)}
            selectProvinsi
            selectItem={cities}
          />
          <Gap height={12} />
          <TextInput
            label="Kecamatan"
            placeholder="Pilih Kecamatan"
            value={biodata.kecamatan}
            onValueChange={value => getVillages(value)}
            selectProvinsi
            selectItem={districts}
          />
          <Gap height={12} />
          <TextInput
            label="Kelurahan"
            placeholder="Pilih Kelurahan"
            value={biodata.kelurahan}
            onValueChange={value => setBiodata('kelurahan', value)}
            selectProvinsi
            selectItem={villages}
          />
          <Gap height={12} />
          <View style={styles.alamat}>
            <View style={styles.form}>
              <TextInput
                label="RW"
                placeholder="Masukkan RW"
                value={biodata.rw}
                style={styles.center}
                keyboardType="numeric"
                onChangeText={value => setBiodata('rw', value)}
              />
            </View>
            <Gap height={12} width={32} />
            <View style={styles.form}>
              <TextInput
                label="RT"
                placeholder="Masukkan RT"
                value={biodata.rt}
                style={styles.center}
                keyboardType="numeric"
                onChangeText={value => setBiodata('rt', value)}
              />
            </View>
          </View>
          <Gap height={12} />
          <TextInput
            label="Kode Pos"
            placeholder="Masukkan kode pos"
            value={biodata.kodePos}
            style={styles.center}
            keyboardType="numeric"
            onChangeText={value => setBiodata('kode_pos', value)}
          />
          <Gap height={30} />
          {biodata.name !== '' &&
          biodata.no_telp !== '' &&
          biodata.tempat_lahir !== '' &&
          biodata.tanggal_lahir !== '' &&
          biodata.alamat !== '' &&
          biodata.provinsi !== '' &&
          biodata.kota !== '' &&
          biodata.kecamatan !== '' &&
          biodata.kelurahan !== '' &&
          biodata.rw !== '' &&
          biodata.rt !== '' &&
          biodata.kode_pos !== '' ? (
            <Button tittle="Selanjutnya" onPress={onContinue} />
          ) : (
            <Button tittle="Selanjutnya" type="disable" disable />
          )}
          <Gap height={50} />
        </View>
      </ScrollView>
    </>
  );
};
export default UpdateBiodata;

const styles = StyleSheet.create({
  icon: {
    position: 'absolute',
    right: -10,
    bottom: -10,
  },
  cal: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
  },
  tgl: {
    flexDirection: 'row',
  },
  inpTgl: {
    width: 300,
  },
  calender: {
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  center: {
    textAlign: 'center',
    borderWidth: 1,
    paddingHorizontal: 30,
    borderRadius: 8,
    paddingVertical: 12,
    borderColor: colors.primary,
    color: colors.primary,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  photo: {
    alignItems: 'center',
    marginTop: 26,
    marginBottom: 16,
  },
  borderPhoto: {
    borderWidth: 1,
    borderColor: '#8D92A3',
    width: 110,
    height: 110,
    borderRadius: 110,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoContainer: {
    width: 90,
    height: 90,
    borderRadius: 90,
    backgroundColor: '#F0F0F0',
    padding: 24,
  },
  addPhoto: {
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: '#8D92A3',
    textAlign: 'center',
  },
  button: {
    height: 50,
  },
  alamat: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  container: {
    backgroundColor: colors.tertiary,
    flex: 1,
    paddingHorizontal: 25,
  },
  form: { flex: 1 },
});
