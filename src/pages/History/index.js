import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Header, HomeTabSelection } from '../../components';

const History = () => {
  return (
    <View style={styles.page}>
      <View style={styles.content}>
        <Header title="Riwayat Anda" center />
        <View style={styles.tabContainer}>
          <HomeTabSelection />
        </View>
      </View>
    </View>
  );
};
export default History;

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  tabContainer: {
    flex: 1,
  },
});
