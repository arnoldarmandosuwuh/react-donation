import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import { ILLBocil } from '../../assets';
import { Button, Gap } from '../../components';
import { colors } from '../../utils';

const GetStarted = ({ navigation }) => {
  return (
    <ImageBackground source={ILLBocil} style={styles.page}>
      <View>
        <Text style={styles.title}>
          Mari berdonasi dengan mudah cepat dan aman
        </Text>
      </View>
      <View style={styles.button}>
        <Button
          type="secondary"
          tittle="Daftar"
          onPress={() => navigation.navigate('Register')}
        />
        <Gap width={36} />
        <Button tittle="Masuk" onPress={() => navigation.replace('Login')} />
      </View>
    </ImageBackground>
  );
};
export default GetStarted;

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {
    fontSize: 50,
    color: colors.tertiary,
  },
  page: {
    flex: 1,
    paddingHorizontal: 40,
    paddingTop: 63,
    paddingBottom: 53,
    justifyContent: 'space-between',
  },
});
