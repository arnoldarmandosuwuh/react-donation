import React from 'react';
import { Image, ScrollView, StyleSheet, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { IcLogoMknKita } from '../../assets';
import { Button, Gap, Header, TextInput } from '../../components';
import { colors, useForm } from '../../utils';

const Register = ({ navigation }) => {
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });

  const dispatch = useDispatch();

  const submit = () => {
    dispatch({ type: 'SET_REGISTER', value: form });
    navigation.navigate('Biodata');
  };
  return (
    <>
      <Header
        title="Daftar"
        deskripsi="Silahkan Mendaftar"
        onBack={() => navigation.goBack()}
      />

      <View style={styles.container}>
        <ScrollView>
          <View style={styles.page}>
            <Image source={IcLogoMknKita} style={styles.logo} />
          </View>
          <TextInput
            label="Email"
            placeholder="Masukkan Email"
            value={form.email}
            onChangeText={value => setForm('email', value)}
          />

          <Gap height={20} />
          <TextInput
            label="Password"
            placeholder="Masukkan Password"
            secureTextEntry
            value={form.password}
            onChangeText={value => setForm('password', value)}
          />

          <Gap height={100} />
          <View style={styles.button}>
            {form.email !== '' && form.password !== '' ? (
              <Button tittle="Selanjutnya" onPress={() => submit()} />
            ) : (
              <Button tittle="Selanjutnya" type="disable" disable />
            )}
          </View>
          <Gap height={50} />
        </ScrollView>
      </View>
    </>
  );
};
export default Register;

const styles = StyleSheet.create({
  page: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    alignItems: 'center',
    width: 200,
    height: 200,
  },
  button: {
    height: 50,
  },
  container: {
    backgroundColor: colors.tertiary,
    flex: 1,
    paddingHorizontal: 25,
  },
});
