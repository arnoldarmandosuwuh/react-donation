import React, { useEffect } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { IcLogoMknKita } from '../../assets';
import { colors, getData } from '../../utils';

const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      getData('token').then(res => {
        if (res) {
          navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
        } else {
          navigation.replace('Corousel');
        }
      });
    }, 2000);
  }, [navigation]);
  return (
    <View style={styles.page}>
      <Image source={IcLogoMknKita} style={styles.logo} />
    </View>
  );
};
export default SplashScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.primary,
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
  },
  logo: {
    width: '90%',
    height: '45%',
  },
});
