import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Header, Button, Gap, TextInput } from '../../components';
import { colors } from '../../utils';

const UpdatePassword = ({ navigation }) => {
  return (
    <>
      <Header
        title="Ganti Password"
        deskripsi="silahkan ganti password baru"
        onBack={() => navigation.goBack()}
      />
      <View style={styles.container}>
        <View>
          <TextInput
            label="Password saat ini"
            placeholder="Masukkan password saat ini"
          />
          <Gap height={20} />
          <TextInput
            label="Password baru"
            placeholder="Masukkan Password baru"
          />
          <Gap height={20} />
          <TextInput
            label="Konfirmasi Password"
            placeholder="Konfirmasi Password"
          />
        </View>
        <View style={styles.button}>
          <Button
            tittle="Perbarui password"
            onPress={() => navigation.navigate('MainApp')}
          />
        </View>
        <Gap height={50} />
      </View>
    </>
  );
};
export default UpdatePassword;

const styles = StyleSheet.create({
  page: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    alignItems: 'center',
    width: '40%',
    height: '40%',
  },
  button: {
    height: 50,
  },
  container: {
    backgroundColor: colors.tertiary,
    justifyContent: 'space-between',
    flex: 1,
    paddingHorizontal: 25,
  },
});
