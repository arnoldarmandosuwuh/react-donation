import React, { useEffect } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { IcLogoMknKita } from '../../assets';
import { colors } from '../../utils';

const SplashDone = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
    }, 2000);
  }, [navigation]);
  return (
    <View style={styles.page}>
      <Image source={IcLogoMknKita} style={styles.logo} />
      <Text style={styles.title}>Terima Kasih</Text>
      <Text style={styles.desc}>Anda telah melakukan donasi</Text>
    </View>
  );
};
export default SplashDone;

const styles = StyleSheet.create({
  title: {
    color: '#FBFF22',
    fontSize: 20,
    fontFamily: 'Poppins-Regular',
  },
  desc: {
    color: 'white',
    fontFamily: 'Poppins-Light',
    fontSize: 14,
  },
  page: {
    backgroundColor: colors.primary,
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
  },
  logo: {
    width: '90%',
    height: '45%',
  },
});
