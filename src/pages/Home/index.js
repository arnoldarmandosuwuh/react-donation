import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  Modal,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { IcDelete } from '../../assets';
import { Button, Card, Gap, ProfileHeader } from '../../components';
import { API_HOST } from '../../config';
import { getItem } from '../../redux/action';
import { colors, getData, showMessage } from '../../utils';

const Home = ({ navigation }) => {
  const { items } = useSelector(state => state.itemsReducer);
  const [modalVisible, setModalVisible] = useState(false);
  const [closeBtn, setCloseBtn] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    navigation.addListener('focus', () => {
      dispatch(getItem());
    });
    
    setTimeout(() => {
      setModalVisible(true);
      setTimeout(() => {
        setCloseBtn(true);
      }, 5000);
    }, 3000);
  }, [navigation, dispatch]);

  const deleteItem = idItem => {
    getData('token').then(resToken => {
      Axios.delete(`${API_HOST.url}/remove-item?id=${idItem}`, {
        headers: {
          Authorization: resToken.value,
        },
      })
        .then(res => {
          navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] });
        })
        .catch(error => {
          showMessage(error?.response?.message || 'Hapus item tidak berhasil');
        });
    });
  };

  const no = () => {
    setModalVisible(!modalVisible);
  };

  return (
    <>
      <Modal animationType="none" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ScrollView>
              <Text>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum
              </Text>
            </ScrollView>
            {closeBtn && (
              <View style={styles.btnStyle}>
                <Pressable style={styles.button} onPress={() => no()}>
                  <View style={styles.btnClose}>
                    <IcDelete />
                  </View>
                </Pressable>
              </View>
            )}
          </View>
        </View>
      </Modal>

      <View style={styles.page}>
        <Button
          icon="cekout"
          disable={items.length === 0 && 'disable'}
          onPress={() => navigation.navigate('DetailDonasi')}
        />
        <Button icon="add" onPress={() => navigation.navigate('AddDonasi')} />
        <ProfileHeader />
        {items.length === 0 ? (
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.title}>
              <Text style={styles.container}>Data Tidak Ada</Text>
            </View>
          </ScrollView>
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            {items.map((item, index) => {
              return (
                <Card
                  key={index}
                  item={item}
                  onPress={() => deleteItem(item.id)}
                />
              );
            })}
            <Gap height={150} />
          </ScrollView>
        )}
      </View>
    </>
  );
};
export default Home;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(52,64,125,0.5)',
  },
  modalView: {
    marginHorizontal: 10,
    marginVertical: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  btnStyle: {
    top: 20,
    position: 'absolute',
    right: 20,
  },
  button: {
    height: 50,
  },
  btnClose: {
    padding: 10,
  },

  title: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  page: {
    backgroundColor: colors.tertiary,
    flex: 1,
    paddingHorizontal: 24,
  },
  container: {
    fontFamily: 'Poppins-Light',
    fontSize: 20,
    color: colors.primary,
  },
});
