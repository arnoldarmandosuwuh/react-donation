import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { ILnasi } from '../../assets';
import { Gap, Header } from '../../components';
import { colors } from '../../utils';

const DetailTransaksi = ({ navigation }) => {
  return (
    <>
      <Header single grey title="Berhasil" onBack={() => navigation.goBack()} />
      <ScrollView style={styles.page}>
        <View style={styles.card}>
          <View>
            <Image source={ILnasi} style={styles.image} />
          </View>
          <View style={styles.text}>
            <Text style={styles.title}>Transaksi berhasil</Text>
            <Text style={styles.tgl}>20 September 2021</Text>
            <Text style={styles.name}>Oleh Jay Nabila</Text>
          </View>
        </View>
        <Gap height={10} />
        <View style={styles.list}>
          <Text style={styles.font}>No. Transaksi</Text>
          <Text style={styles.resi}>JK5110404559</Text>
        </View >
        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>

        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>

        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>
        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>
        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>
        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>
        <View style={styles.list}>
          <View style={styles.tglTransaksi}>
            <Text style={styles.font}>18 September 2021</Text>
            <Text style={styles.font}>19:55:49</Text>
          </View>
          <Text style={styles.font}>Oleh Jay Nabila</Text>
        </View>
        <Gap height={30} />
      </ScrollView>
    </>
  )
}
export default DetailTransaksi;

const styles = StyleSheet.create({
  tglTransaksi: {
    alignItems: 'flex-end'
  },
  font: {
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: colors.primary,
  },
  resi: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
  },
  list: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    backgroundColor: colors.seventh,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  text: {
    paddingLeft: 20,
    justifyContent: 'center'
  },
  title: {
    color: colors.primary,
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
  },
  tgl: {
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
    fontSize: 16,
  },
  name: {
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    color: colors.primary,
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 8,
  },
  card: {
    flexDirection: 'row',
    backgroundColor: colors.seventh,
    paddingLeft: 25,
    paddingVertical: 30,
    paddingRight: 10
  },
  page: {
    backgroundColor: colors.tertiary,
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
})
